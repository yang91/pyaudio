# -*- coding: utf-8 -*-

# Sample Python code for youtube.videos.list
# See instructions for running these code samples locally:
# https://developers.google.com/explorer-help/guides/code_samples#python

import os
import json
import pathlib
import zipfile

from tqdm import tqdm

from download import download_from_video_id

#import googleapiclient.discovery

from youtube_search import YoutubeSearch


def download_youtube_videos_from_response(result, output_path="./data"):
    items = result["items"]
    pbar = tqdm(items)
    for item in pbar:
        #title = item["snippet"]["title"]
        #channel_title = item["snippet"]["channelTitle"]
        video_id = item["id"].get("videoId")
        if video_id:
            opts = {
                "output_path": output_path,
                "filename": video_id
            }
            try:
                path = download_from_video_id(video_id, **opts)
            except (KeyError, Exception) as err:
                print(err, video_id)
            else:
                if os.path.isfile(path):
                    filename = os.path.basename(path)
                    target_path = os.path.join(output_path, filename)
                    item["id"]["targetPath"] = target_path
        pbar.set_description("Download Youtube videos")
    return result

def download_youtube_videos_without_api(results, output_path="./data"):
    items = results["videos"]
    try:
        pbar = tqdm(items, leave=False, position=2)
        for item in pbar:
            pbar.set_description("Download Youtube videos without API")
            video_id = item.get("id")
            if video_id:
                opts = {
                    "output_path": output_path,
                    "filename": video_id
                }
                try:
                    path = download_from_video_id(video_id, **opts)
                except (KeyError, Exception) as err:
                    #print(err, video_id)
                    pass
                else:
                    if os.path.isfile(path):
                        filename = os.path.basename(path)
                        target_path = os.path.join(output_path, filename)
                        item["target_path"] = target_path
    finally:
        pbar.close()
    return results

def search_youtube_videos_without_api(query, max_results=50):
    results = YoutubeSearch(query, max_results=max_results).to_json()
    return json.loads(results)


def search_youtube_videos(youtube, query, max_results=5, **kwargs):
    request = youtube.search().list(
        part="snippet",
        q=query,
        maxResults=max_results,
        type="video",
        **kwargs
    )
    response = request.execute()
    return response

def search_related_youtube_videos_to_video_id(youtube, video_id):
    request = youtube.search().list(
        part="snippet",
        relatedToVideoId=video_id,
        type="video"
    )
    response = request.execute()
    return response

def write_json(filename, obj, indent=2, sort_keys=True, **kwargs):
    with open(filename, "w") as f:
        json.dump(obj, f, indent=indent, sort_keys=sort_keys, **kwargs)
    
def search_youtube_video_resources(youtube, ids: list):
    request = youtube.videos().list(
        part="snippet,status,contentDetails,statistics",
        id=','.join(ids),
    )
    response = request.execute()
    return response

def get_video_ids_from_response(response):
    ids = [
        item["id"]["videoId"]
        for item in response["items"]
        if "videoId" in item["id"].keys()
        ]
    return ids
    
def download_related_youtube_videos(youtube, video_ids, current_level: int, level: int):
    if current_level == level:
        return None
    for video_id in video_ids:
        _result = search_related_youtube_videos_to_video_id(youtube, video_id)
        _video_ids = get_video_ids_from_response(_result)
        _resource_response = search_youtube_video_resources(youtube, _video_ids)
        _result = download_youtube_videos_from_response(_result)

        filename = f"youtube-response-{video_id}.json"
        write_json(filename, _result)
        filename = f"youtube-resource-response-{video_id}.json"
        write_json(filename, _resource_response)

        download_related_youtube_videos(youtube, _video_ids, current_level + 1, level)


def main():
    # Disable OAuthlib's HTTPS verification when running locally.
    # *DO NOT* leave this option enabled in production.
    #os.environ["OAUTHLIB_INSECURE_TRANSPORT"] = "1"

    api_service_name = "youtube"
    api_version = "v3"
    DEVELOPER_KEY = "AIzaSyDKSwKwdlae--kkbALuu1-BOxQxGTdXZSM"

    youtube = googleapiclient.discovery.build(
        api_service_name, api_version, developerKey = DEVELOPER_KEY)

    max_results = 50
    queries = [
        "中國國防",
        "中國訪問",
        "iraqi prime minister interview",
        "erna solberg interview"
        ]

    with tqdm(queries, position=1) as pbar:
        for i, query in enumerate(pbar):
            opts = {
                "max_results": max_results,
            }
            #response = search_youtube_videos(youtube, query, **opts)
            response = search_youtube_videos_without_api(query, **opts)

            #video_ids = get_video_ids_from_response(response)
            #resource_response = search_youtube_video_resources(youtube, video_ids)

            result = download_youtube_videos_without_api(response)

            filename = f"youtube-response-{i}.json"
            write_json(filename, result)
            #filename = f"youtube-resource-response-{i}.json"
            #write_json(filename, resource_response)

            #download_related_youtube_videos(youtube, video_ids, 0, 1)

    with zipfile.ZipFile("youtube-data.zip", mode="w") as zf:
        paths = pathlib.Path(".").rglob("youtube-response-*.json")
        with tqdm(paths, position=1) as pbar:
            pbar.set_description("Compress files")
            for p in paths:
                zf.write(str(p))

        paths = pathlib.Path("./data").rglob("*")
        with tqdm(paths, position=1) as pbar:
            pbar.set_description("Compress data files")
            for p in paths:
                zf.write(str(p))


if __name__ == "__main__":
    main()
