from pytube import YouTube

def download_from_video_id(video_id, **kwargs):
    yt = YouTube(f'http://www.youtube.com/watch?v={video_id}')
    path = yt.streams \
        .filter(progressive=True, file_extension="mp4") \
        .order_by("resolution") \
        .asc() \
        .first() \
        .download(**kwargs)
    return path


